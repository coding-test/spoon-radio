module.exports = {

    development: {
        client: 'mysql',
        connection: {
            database: 'spoon',
            user:     'spoon',
            password: 'spoon'
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        },
        seeds: {
            directory: './seeds'
        },
    },

    testing: {
        client: 'mysql',
        connection: {
            database: 'spoon_testing',
            user:     'spoon',
            password: 'spoon'
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        },
        seeds: {
            directory: './seeds'
        },
    },

    production: {
        client: 'mysql',
        connection: {
            database: 'spoon',
            user:     'spoon',
            password: 'spoon'
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        },
        seeds: {
            directory: './seeds'
        },
    },

};
