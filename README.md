## Spoon Radio coding test

### How to run

- Confirmed Working with : 
 - Ubuntu 18.04
 - Nodejs v12.16.3 
 - MariaDB 10.4+

----

- 섪치 : `yarn | npm install`
- 셋업 : `yarn | npm setup` 
- 시작 :  `yarn | npm start`

### Documentation 

- 먼저 doc 빌드 함 : `yarn | npm build:doc`

API 설명서를 `doc/` 폴더에 나옴.

### Testing

#### Unit / Integration

TDD 씩으로 개발 했으며 테스팅 가능 합니다 :
- `yarn test --watch`

### Directory Structure
  
 /doc            설명서 폴더
 /migrations     DB 스키머 
 /seeds          DB 데이타 
 /src            서버 코드
   /config           Server setup
   /controllers      Controling logic
   /helpers          Various helping functions
   /middlewares      Express middlewares
   /models           Database models
   /routes           Listening routes
 /test           테스트 코드
 
### 설명 / 부족한 부분 

- Fan count 부분이 Channel 테이블에서 cache value를 추가 하려고 했음. 
- 한글 설명을 부족함
- OAuth 코드가 깔끔하지 않음
- Postman Collection 추가 못 했음 
- Load test 추가 못했음 (100K users, 20K channels, 10K live, 500K fan per channel)
