process.env.NODE_ENV = 'testing'

const chai = require('chai')
const expect = chai.expect

chai.use(require('chai-http'))

const server = require('../src')


describe('OAuth', () => {

    it('should return a token on login', async () => {

        const { body, ...res } = await chai.request(server)
            .post('/users/login')
            .set('Content-Type','application/x-www-form-urlencoded')
            .send({ 
                username: 'test1@example.com', 
                password:'1234',
                client_id: 'application',
                client_secret: '123456789',
                grant_type:'password',
            })

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body).to.be.an('object')
        expect(body).to.have.property('accessToken')
        expect(body).to.have.property('refreshToken')

        return Promise.resolve()

    })

})
