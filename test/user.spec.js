process.env.NODE_ENV = 'testing'

const chai = require('chai')
const expect = chai.expect

chai.use(require('chai-http'))

const server = require('../src')



describe('Users', () => {


    let token

    before(async () => {

        const { body, ...res } = await chai.request(server)
            .post('/users/login')
            .set('Content-Type','application/x-www-form-urlencoded')
            .send({ 
                username: 'test1@example.com', 
                password:'1234',
                client_id: 'application',
                client_secret: '123456789',
                grant_type:'password',
            })

        token = body

    })

    it('should allow to block an user',  async () => {

        const { body, ...res } = await chai.request(server)
            .post('/users/3/block')
            .set('Authorization', `Bearer ${token.accessToken}`)

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body).to.be.an('object')
        expect(body).to.have.property('blocked')
        expect(body.blocked).to.be.true

        return Promise.resolve()

    })

    it('should allow to unblock an user',  async () => {

        const { body, ...res } = await chai.request(server)
            .post('/users/3/unblock')
            .set('Authorization', `Bearer ${token.accessToken}`)

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body).to.be.an('object')
        expect(body).to.have.property('blocked')
        expect(body.blocked).to.be.false


        return Promise.resolve()

    })

})
