process.env.NODE_ENV = 'testing'

const chai = require('chai')
const expect = chai.expect

chai.use(require('chai-http'))

const server = require('../src')


describe('Fan', () => {

    let token

    before(async () => {

        const { body, ...res } = await chai.request(server)
            .post('/users/login')
            .set('Content-Type','application/x-www-form-urlencoded')
            .send({ 
                username: 'test4@example.com', 
                password:'1234',
                client_id: 'application',
                client_secret: '123456789',
                grant_type:'password',
            })

        token = body

    })

    it('should allow to fan a channel',  async () => {

        const { body, ...res } = await chai.request(server)
            .post('/channels/1/fan')
            .set('Authorization', `Bearer ${token.accessToken}`)

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body).to.be.an('object')
        expect(body).to.have.property('fan')
        expect(body.fan).to.be.true

        return Promise.resolve()

    })

    it('should allow to unfan a channel',  async () => {

        const { body, ...res } = await chai.request(server)
            .post('/channels/1/unfan')
            .set('Authorization', `Bearer ${token.accessToken}`)

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body).to.be.an('object')
        expect(body).to.have.property('fan')
        expect(body.fan).to.be.false

        return Promise.resolve()

    })

    it('should not count blocked users as fans',  async () => {

        const { body, ...res } = await chai.request(server)
            .get('/channels/1')

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body).to.be.an('object')
        expect(body).to.have.property('id')
        expect(body).to.have.property('name')
        expect(body).to.have.property('user_id')
        expect(body.fans.count).to.equal(0)

        return Promise.resolve()

    })

})
