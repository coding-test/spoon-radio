process.env.NODE_ENV = 'testing'

const chai = require('chai')
const expect = chai.expect

chai.use(require('chai-http'))

const server = require('../src')


describe('Channels', () => {

    let token

    before(async () => {

        const { body, ...res } = await chai.request(server)
            .post('/users/login')
            .set('Content-Type','application/x-www-form-urlencoded')
            .send({ 
                username: 'test2@example.com', 
                password:'1234',
                client_id: 'application',
                client_secret: '123456789',
                grant_type:'password',
            })

        token = body

    })


    it('should allow to create a channel',  async () => {

        const { body, ...res } = await chai.request(server)
            .post('/channels/create')
            .set('Authorization', `Bearer ${token.accessToken}`)
            .send({name:'My Channel'})

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body).to.be.an('object')
        expect(body.name).to.equal('My Channel')

        return Promise.resolve()

    })

    it('should set the start flag',  async () => {

        const { body, ...res } = await chai.request(server)
            .post('/channels/mychannel/start')
            .set('Authorization', `Bearer ${token.accessToken}`)
            .send()

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body.started).to.be.true

        return Promise.resolve()

    })


    it('should set the end flag',  async () => {

        const { body, ...res } = await chai.request(server)
            .post('/channels/mychannel/end')
            .set('Authorization', `Bearer ${token.accessToken}`)
            .send()

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body.ended).to.be.true

        return Promise.resolve()

    })

    it('should return the list of existing channels', async () => {

        const { body, ...res } = await chai.request(server)
            .get('/channels/list')

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body.channels).to.be.an('array')
        expect(body.channels[0]).to.have.property('id')
        expect(body.channels[0]).to.have.property('name')
        expect(body.channels[0]).to.have.property('user_id')

        return Promise.resolve()

    })

    it('should return a specific channel (visitor, without fan list)',  async () => {

        const { body, ...res } = await chai.request(server)
            .get('/channels/3')

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body).to.be.an('object')
        expect(body).to.have.property('id')
        expect(body).to.have.property('name')
        expect(body).to.have.property('user_id')
        expect(body.fans).to.have.property('count')
        expect(body.fans).to.not.have.property('users')

        return Promise.resolve()

    })
    
    it('should return a specific channel (logged-in, with fan list)',  async () => {

        const { body, ...res } = await chai.request(server)
            .get('/channels/3')
            .set('Authorization', `Bearer ${token.accessToken}`)

        expect(res).to.have.status(200)
        expect(res).to.be.json 
        expect(body).to.be.an('object')
        expect(body).to.have.property('id')
        expect(body).to.have.property('name')
        expect(body).to.have.property('user_id')
        expect(body.fans).to.have.property('users')
        expect(body.fans.users).to.be.an('array')

        return Promise.resolve()

    })
    
    it('should not show channels owned by blocked users',  async () => {

        const { body, ...res } = await chai.request(server)
            .get('/channels/1')
            .set('Authorization', `Bearer ${token.accessToken}`)
            .send({name:'My Channel'})

        expect(res).to.have.status(404)
        expect(body.message).to.equal('ChannelNotFound')

        return Promise.resolve()

    })

})
