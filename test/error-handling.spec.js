process.env.NODE_ENV = 'testing'

const chai = require('chai')
const expect = chai.expect

chai.use(require('chai-http'))

const server = require('../src')


describe('Error handling', () => {

    let token 

    before(async () => {

        const { body, ...res } = await chai.request(server)
            .post('/users/login')
            .set('Content-Type','application/x-www-form-urlencoded')
            .send({ 
                username: 'test3@example.com', 
                password:'1234',
                client_id: 'application',
                client_secret: '123456789',
                grant_type:'password',
            })

        token = body

    })

    it('should not allow login with wrong password', async () => {

        const { body, ...res } = await chai.request(server)
            .post('/users/login')
            .set('Content-Type','application/x-www-form-urlencoded')
            .send({ 
                username: 'test1@example.com', 
                password:'12345',
                client_id: 'application',
                client_secret: '123456789',
                grant_type:'password',
            })

        expect(res).to.have.status(401)

        return Promise.resolve()

    })

    it('should prevent access to protected endpoint with an unknown token', async () => {

        const { body, ...res } = await chai.request(server)
            .post('/channels/create')
            .set('Authorization', `Bearer 1234`)
            .send({name:'My Channel'})

            expect(res).to.have.status(401)

        return Promise.resolve()

    })


    it('should prevent blocking myself', async () => {

        const { body, ...res } = await chai.request(server)
            .post('/users/3/block')
            .set('Authorization', `Bearer ${token.accessToken}`)
            .send({name:'My Channel'})

            expect(res).to.have.status(409)

        return Promise.resolve()

    })

    it('should prevent channel creation if not authenticated', async () => {

        const { body, ...res } = await chai.request(server)
            .post('/channels/create')
            .set('Authorization', `Bearer 1234`)
            .send({name:'My Channel'})

            expect(res).to.have.status(401)

        return Promise.resolve()

    })




})
