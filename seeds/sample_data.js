const bcrypt = require('bcrypt')

exports.seed = async function(knex) {

    /* Clear all existing data */

    await knex('oauth_tokens').del()
    await knex('oauth_clients').del()
    await knex('blocking').del()
    await knex('fan').del()
    await knex('channels').del()
    await knex('users').del()

    /* 
     * Inserting 4 users : 
     *  - User 1, DJ of channel 1
     *  - User 2, fan of channel 1
     *  - User 3, fan channel 1 & blocked from User 1
     *  - User 4, blocking User 1 
     */

    const password = bcrypt.hashSync('1234', 10)

    await knex('users').insert([
        {id: 1, email: 'test1@example.com', password },
        {id: 2, email: 'test2@example.com', password },
        {id: 3, email: 'test3@example.com', password },
        {id: 4, email: 'test4@example.com', password },
    ]);

    await knex('channels').insert([
        {user_id: 1, name: 'channel 1', started_at: null},
        {user_id: 3, name: 'channel 3', started_at: new Date()},
    ]);

    await knex('fan').insert([
        {user_id: 2, channel_id: 1},
        {user_id: 3, channel_id: 1},
    ]);

    await knex('blocking').insert([
        {user_id: 1, target_user_id: 3},
        {user_id: 2, target_user_id: 1},
        {user_id: 4, target_user_id: 1},
    ]);


    /* OAuth client */
    await knex('oauth_clients').insert([
        {
            clientId:'application',
            clientSecret:'123456789',
            grants:'["password","refresh_token"]',
            redirectUris:'[]',
        }
    ])

    return Promise.resolve()
};
