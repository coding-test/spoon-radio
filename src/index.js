const express = require('express')
const bodyParser = require('body-parser')
const OAuth2 = require('oauth2-server')
const OAuthModel = require('helpers/OAuthModel')
const { handleError } = require('helpers/error')
const routes = require('routes/')

const app = express()

/* OAuth2 Authentication */
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.oauth = new OAuth2({
    debug: false,
    model: OAuthModel,
    accessTokenLifetime: 1800,
    refreshTokenLifetime: 3600 * 24,
    requireClientAuthentication:false,
})

/* Application routes */
app.use('/', routes)

/* Not handled routes are defined as 404 errors */
//app.use('*', (req, res) => res.status(404).json({message:"Not Found"}))
app.use((err, req, res, next) => {
    handleError(err, res)
})

/* Avoiding mocha --watch failing with address in use error */
if (!module.parent) {
    app.listen(process.env.PORT || 3000, () => {
        console.table({ 
            service: `Spoon Test`,
            environment: process.env.NODE_ENV || 'development',
            port: process.env.PORT || 3000,
        })
    })
}

module.exports = app
