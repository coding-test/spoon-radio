const bcrypt = require('bcrypt')
const { ErrorHandler } = require('helpers/error')
const { 
    Token, 
    Client, 
    User 
} = require('models')

/*
 * Methods used by all grant types.
 */

const getAccessToken = async function (accessToken) {

    let tokenData

    try {
        const rawToken = await Token.findOne(function() {
            this.where({accessToken})
                .andWhere(
                    'accessTokenExpiresAt', 
                    '>', 
                    Date.now()
                )
        })

        if (!rawToken) throw new Error("Invalid Token")

        tokenData = {...rawToken}
        const { accessTokenExpiresAt, refreshTokenExpiresAt } = rawToken

    }catch(e) {
        throw new ErrorHandler(503, e.message)
    }

    if (accessToken !== tokenData.accessToken) {
        throw new Error('Invalid token')
    }

    const user = await User.findOne({
        id: tokenData.userId
    })

    delete(user.password)

    return {
        ...tokenData,
        user
    }
}

const getRefreshToken = async function (token) {

    throw new Error("Removed for simplification") 

}

const revokeToken = async function (token) {

    throw new Error("Removed for simplification") 

}


/* Always returning the first client for this project */
const getClient = async function(clientId, clientSecret) {

    const client = await Client.findOne({id:1})
    client.grants = JSON.parse(client.grants)

    return client
}

const saveToken = async function (token, client, user) {

    const { scope, ...tokenData } = token
    const tokenProps = {
       ...tokenData, 
        userId: user.id, 
        clientId: client.id,
        userId: user.id,
    }

    const savedToken = await Token.create(tokenProps)

    return {
        ...tokenProps,
        client: {...client},
        user: {...user},
    }
}

/*
 * Method used only by password grant type.
 */

const getUser = async function(username, password) {

    const result = await User.findOne({email: username})

    if (undefined === result.email) {
        throw new Error("Not Found")
    }

    if (!bcrypt.compareSync(password, result.password)) {
        throw new Error("Wrong password")
    }

    return result
}

/*
 * Method used only by client_credentials grant type.
 */

const getUserFromClient = async function ({ clientId, clientSecret }) {

    throw new Error("Removed for simplification")

}

/**
 * Export model definition object.
 */

module.exports = {
    getAccessToken,
    getRefreshToken,
    getClient,
    saveToken,
    getUser,
    getUserFromClient,
    revokeToken,
}


