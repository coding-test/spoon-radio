'use strict'

const createGuts = require('helpers/model-guts')

const name = 'Fan'
const tableName = 'fan'

const selectableProps = [
    'id',
    'user_id',
    'channel_id',
]

module.exports = knex => {
    const guts = createGuts({
        knex,
        name,
        tableName,
        selectableProps
    })

    return {
        ...guts
    }
}
