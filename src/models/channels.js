'use strict'

const createGuts = require('helpers/model-guts')

const name = 'Channel'
const tableName = 'channels'

const selectableProps = [
    'id',
    'user_id',
    'name',
    'started_at',
    'ended_at',
]

module.exports = knex => {
    const guts = createGuts({
        knex,
        name,
        tableName,
        selectableProps
    })

    /* Knex queries are not very elegant... switching back to raw MySQL  */
    const channelWithFans = channel_id => knex.raw(`
        SELECT u.email, f.user_id,  f.channel_id 
        FROM channels c 
        LEFT JOIN fan AS f ON c.id = f.channel_id
        LEFT JOIN users AS u ON f.user_id = u.id
        WHERE f.channel_id = ? 
        AND NOT EXISTS(
            SELECT 1 FROM blocking b 
            WHERE (b.user_id = f.user_id AND target_user_id = c.user_id)
            OR (b.user_id = c.user_id AND target_user_id = f.user_id)
        ) 
        AND f.user_id <> 1

    `, [channel_id]).then(result => result[0])

    const createUserChannel = (user_id, name) => knex.raw(
        guts.create({ user_id, name })
            .toString() + ' ON DUPLICATE KEY UPDATE deleted_at = NULL'
        )

    const getFullChannel = async ({channel_id, user_id}) => {

        /* Get channel meta data */
        const channelData = await guts.findOne({id: channel_id})

        if (!channelData || !channelData.id) throw new Error('ChannelNotFound')

        /* Get channel extended info */
        const fanData = await channelWithFans(channelData.id)

        return {
            ...channelData,
            fans:{
                count: fanData.length,
                users: user_id === channelData.user_id ? fanData : undefined
            }
        }

    }
    
    return {
        ...guts,
        channelWithFans,
        createUserChannel,
        getFullChannel,
    }
}
