'use strict'

const createGuts = require('helpers/model-guts')

const name = 'Blocking'
const tableName = 'blocking'

const selectableProps = [
    'id',
    'user_id',
    'target_user_id',
]

module.exports = knex => {
    const guts = createGuts({
        knex,
        name,
        tableName,
        selectableProps
    })

    return {
        ...guts
    }
}
