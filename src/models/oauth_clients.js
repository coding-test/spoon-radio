'use strict'

const createGuts = require('helpers/model-guts')

const name = 'Client'
const tableName = 'oauth_clients'

const selectableProps = [
    'id',
    'clientId',
    'clientSecret',
    'grants',
    'redirectUris',
]

module.exports = knex => {
    const guts = createGuts({
        knex,
        name,
        tableName,
        selectableProps
    })

    return {
        ...guts
    }
}
