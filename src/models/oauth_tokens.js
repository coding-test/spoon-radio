'use strict'

const createGuts = require('helpers/model-guts')

const name = 'Token'
const tableName = 'oauth_tokens'

const selectableProps = [
    'id',
    'accessToken',
    'accessTokenExpiresAt',
    'refreshToken',
    'refreshTokenExpiresAt',
    'clientId',
    'userId',
]

module.exports = knex => {
    const guts = createGuts({
        knex,
        name,
        tableName,
        selectableProps
    })

    return {
        ...guts
    }
}
