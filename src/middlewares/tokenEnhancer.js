const OAuth2Server = require('oauth2-server')

const { Request } = OAuth2Server
const { Response } = OAuth2Server

module.exports = async (req, res, next) => {
    const request = new Request(req);
    const response = new Response(res);

    try {
        const token = await req.app.oauth.authenticate(request, response)
        req.user = token.user
        next()
    }catch(err) {
        next()
    }

}
