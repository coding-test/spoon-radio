const OAuth2Server = require('oauth2-server')
const bcrypt = require('bcrypt')
const { ErrorHandler } = require('helpers/error')

const { Channel, Fan, User, Blocking } = require('models')
const { Request } = OAuth2Server
const { Response } = OAuth2Server


/**
 * @api {post} /user/login Authenticate user
 * @apiName LoginUser
 * @apiGroup User
 *
 * @apiParam {string} username Users' username (email)
 * @apiParam {string} password User's password
 *
 * @apiSuccess {User} user User object
 * @apiSuccess {Token} token OAuth2 token
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "user": true,
 *       "token": {}
 *     }
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */

const login = async (req, res, next) => {

    const request = new Request(req)
    const response = new Response(res)
    let token

    try {
        token = await req.app.oauth.token(request, response)

        const { client, user, ...tokenData } = token
        const { clientId, userId, ...tokenProps } = tokenData

        res.json(tokenProps)

    } catch(e) {
        next(new ErrorHandler(401, 'WrongCredentials'))
    }

}

/**
 * @api {post} /user/block/:id Block an user
 * @apiName BlockUser
 * @apiGroup User
 *
 * @apiParam {id} id Users unique ID.
 *
 * @apiSuccess {boolean} blocked Blocked status
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "blocked": true
 *     }
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */

const block = async (req, res, next) => {

    let target_user_id, user_id

    try {
        target_user_id = parseInt(req.params.target_user_id)
        user_id = parseInt(req.user.id)
    }catch(e) {
        return next(new ErrorHandler(400, "InvalidUserId"))
    }

    if (target_user_id === user_id) {
        return next(new ErrorHandler(409, "NoBlockMyself"))
    }

    try {

        /* Actually blocking */
        const rawQuery = Blocking.create({
            user_id,
            target_user_id,
        }).toString() + ` ON DUPLICATE KEY UPDATE user_id = ?`

        await Blocking.knex.raw(rawQuery, [user_id])

        /* Removing fan values in my channel*/

        const myChannelInfo = await Channel.findOne({user_id})

        await Fan.destroyWhere({ 
            channel_id: myChannelInfo.id, 
            user_id: target_user_id
        })

        /* Removing fan values in target's channel*/

        const targetChannelInfo = await Channel.findOne({user_id: target_user_id})

        await Fan.destroyWhere({ 
            channel_id: targetChannelInfo.id, 
            user_id
        })

        res.json({
            blocked: true,
        })

    } catch(e) {
        console.error(e)
        next(new ErrorHandler(500, "ServerError"))
    }

}


/**
 * @api {post} /user/unblock/:id Unblock an user
 * @apiName UnblockUser
 * @apiGroup User
 *
 * @apiParam {id} id Users unique ID.
 *
 * @apiSuccess {boolean} blocked Blocked status
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "blocked": false
 *     }
 *
 * @apiError UserNotFound The id of the User was not found.
 * @apiError UnblockingFailed The unblocking process did not succeed.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "UserNotFound"
 *     }
 */

const unblock = (req, res) => {

    const { target_user_id } = req.params
    const { user } = req
    
    try {

        const result = Blocking.destroyWhere({
            user_id: user.id,
            target_user_id,
        })

        res.json({
            blocked: false,
        })

    } catch(e) {
        console.error(e)
        next('UnblockingFailed')
    }
}


module.exports = {
    login,
    block,
    unblock
}
