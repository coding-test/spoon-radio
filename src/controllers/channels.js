const { ErrorHandler } = require('helpers/error')
const { 
    Blocking,
    Channel, 
    Fan 
} = require('models')

/**
 * @api {get} /list Get all channels
 * @apiName GetChannels
 * @apiGroup Channel
 *
 * @apiSuccess [channel] channel Channel Object
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [{
 *       "id":123,
 *       "user_id":456,
 *       "name": "나야나",
 *       "fans": {
 *           count:0,
 *           users:[],
 *     }]
 *
 */

const list = async (req, res, next) => {

    try {

        /* Not returning soft-deleted records */
        const query = await Channel.find(function() {
            this.whereNull('deleted_at')
        })

        return res.json({
            channels:query,
            count:query.length
        })

    } catch(e) {
        next(new ErrorHandler(500, "ServerError"))
    }

}

/**
 * @api {post} /create Create my channel
 * @apiName CreateChannel
 * @apiGroup Channel
 *
 * @apiParam {string} name Channel name
 *
 * @apiSuccess {number} id Channel id
 * @apiSuccess {number} user_id User (owner) id
 * @apiSuccess {string} Channel name
 * @apiSuccess {object} fans Fans object
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id":123,
 *       "user_id":456,
 *       "name": "나야나",
 *       "fans": {
 *           count:0,
 *           users:[],
 *       }
 *     }
 *
 * @apiError InvalidChannelName Channel name is invalid
 * @apiError ChannelExists Channel already created
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "InvalidChannelName"
 *     }
 */

const create = async (req, res, next) => {

    const { user } = req
    const { name } = req.body

    if (!name) {
        return next(new ErrorHandler(400, "EmptyName"))
    }

    if (!user) {
        return next(new ErrorHandler(400, "EmptyUser"))
    }

    try {

        await Channel.createUserChannel(user.id, name)

        /* Retrieving channel infos */
        const newChannel = await Channel.findOne({user_id: user.id})
        const { id } = newChannel

        res.json({
            id,
            user_id: user.id,
            name,
            fans:{
                count:0,
                users:[]
            }
        })

    } catch(e) {
        next(new ErrorHandler(500, "ServerError"))
    }

}

/**
 * @api {get} /channel Show my channel's profile
 * @apiName MyChannel
 * @apiGroup Channel
 *
 * @apiSuccess {number} id Channel id
 * @apiSuccess {number} user_id User (owner) id
 * @apiSuccess {string} Channel name
 * @apiSuccess {object} fans Fan object
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id":123,
 *       "user_id":456,
 *       "name": "나야나",
 *       "fans": {
 *           count:2,
 *           users:[
 *              7,
 *              3
 *           ],
 *       }
 *     }
 *
 * @apiError ChannelNotFound User has not created its channel yet
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ChannelNotFound"
 *     }
 */

const myChannel = async (req, res, next) => {

    const { user } = req

    try {

        const channelData = await Channel.findOne({user_id:user.id})

        const fanData = await Channel.channelWithFans(channelData.id)

        const { id, name } = {}

        res.json({
            ...channelData,
            fans:{
                count: fanData.length,
                users: channelData.user_id === user.id ? fanData : undefined
            }
        })

    } catch(e) {
        next(new ErrorHandler(500, "ServerError"))
    }

}



/**
 * @api {get} /channel/:id Show channel's profile
 * @apiName ChannelProfile
 * @apiGroup Channel
 *
 * @apiParam {id} id User id owning the channel 
 *
 * @apiSuccess {number} id Channel id
 * @apiSuccess {number} user_id User (owner) id
 * @apiSuccess {string} Channel name
 * @apiSuccess {object} fans Fan object (count only)
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id":123,
 *       "user_id":456,
 *       "name": "나야나",
 *       "fans": {
 *           count:2,
 *       }
 *     }
 *
 * @apiError ChannelNotFound Channel does not exists or is blocked
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ChannelNotFound"
 *     }
 */

const channel = async (req, res, next) => {

    try {
        const { channel_id } = req.params
        const { user } = req


        const channelInfo = await Channel.getFullChannel({
            user_id: req.user ? req.user.id : undefined,
            channel_id,
        })

        /* Check if user is blocked */
        if (user && user.id) {

            const blockingData = await Blocking.findOne(function() {
                    this.where({user_id: user.id, target_user_id: channelInfo.user_id }) 
                        .orWhere({user_id: channelInfo.user_id, target_user_id: user.id }) 
                })

            if (blockingData && blockingData.id) {
                return next( new ErrorHandler(404, 'ChannelNotFound'))
            }

        }

        return res.json(channelInfo)

    } catch(e) {
        next(new ErrorHandler(500, e.message))
    }

}



/**
 * @api {post} /channel/:id/fan Fan a channel
 * @apiName FanChannel
 * @apiGroup Fan
 *
 * @apiParam {number} id Channel id to fan
 *
 * @apiSuccess {boolean} fan Fan status
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "fan": true
 *     }
 *
 * @apiError ChannelNotFound The channel was not found
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ChannelNotFound"
 *     }
 */

const fan = async (req, res, next) => {    

    const { channel_id } = req.params
    const { user } = req

    try {

        const rawQuery = await Fan.create({
            user_id: user.id,
            channel_id,
        }).toString() + 'ON DUPLICATE KEY UPDATE user_id = ?'
        const result = Fan.knex.raw(rawQuery, [user.id])

        res.json({
            fan: true,
        })

    } catch(e) {
        next(new ErrorHandler(500, "ServerError"))
    }
}



/**
 * @api {post} /channel/:id/unfan unfan a channel
 * @apiName UnfanChannel
 * @apiGroup Fan
 *
 * @apiParam {number} id Channel id to unfan
 *
 * @apiSuccess {boolean} fan Fan status
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "fan": false
 *     }
 *
 * @apiError UserNotFound The channel was not found
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ChannelNotFound"
 *     }
 */

const unfan = async (req, res, next) => {

    const { channel_id } = req.params
    const { user } = req

    try {

        const result = await Fan.destroyWhere({
            user_id: user.id,
            channel_id,
        })

        res.json({
            fan: false,
        })

    } catch(e) {
        console.error(e)
        next(new ErrorHandler(500, "ServerError"))
    }
}

/**
 * @api {post} /channel/mychannel/start start broadcasting
 * @apiName StartBroadcast
 * @apiGroup Broadcasting
 *
 * @apiSuccess {boolean} fan Fan status
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "started": true
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ServerError"
 *     }
 */

const start = async (req, res, next) => {
    const { user } = req

    try {
        const channelData = await Channel.findOne({user_id:user.id})
        await Channel.update(channelData.id, {
            started_at: new Date(),
            ended_at: null
        })

        res.json({
            started: true,
        })

    } catch(e) {
        next(new ErrorHandler(500, "ServerError"))
    }
}


/**
 * @api {post} /channel/mychannel/end end broadcasting
 * @apiName EndBroadcast
 * @apiGroup Broadcasting
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "ended": true
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "ServerError"
 *     }
 */

const end = async (req, res, next) => {

    const { user } = req

    try {
        const channelData = await Channel.findOne({user_id:user.id})
        await Channel.update(channelData.id, {ended_at:new Date()})

        res.json({
            ended: true,
        })

    } catch(e) {
        next(new ErrorHandler(500, "ServerError"))
    }
}




module.exports = {
    list,
    create,
    channel,
    myChannel,
    fan, 
    unfan,
    start,
    end
}
