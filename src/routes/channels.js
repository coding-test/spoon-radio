const router = require('express').Router()
const controller = require('controllers/channels')
const tokenAuth = require('middlewares/tokenAuth')
const tokenEnhancer = require('middlewares/tokenEnhancer')

/* Authenticated routes */
router.use(
    [
        '/mychannel',
        '/create',
        '/:channel_id/fan',
        '/:channel_id/unfan',
    ],
    tokenAuth
)

/* Enhance result with user data if exists */
router.use(['/:channel_id'], tokenEnhancer)


router.get('/mychannel', controller.myChannel)
router.post('/mychannel/start', controller.start)
router.post('/mychannel/end', controller.end)
router.post('/create', controller.create)
router.post('/:channel_id/fan', controller.fan)
router.post('/:channel_id/unfan', controller.unfan)


/* Unauthenticated routes */

router.get('/list', controller.list)
router.get('/:channel_id', controller.channel)


module.exports = router
