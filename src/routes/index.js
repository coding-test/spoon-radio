const express = require('express')
const router = express.Router()
const channels = require('routes/channels')
const users = require('routes/users')

router.use('/channels', channels)
router.use('/users', users)

module.exports = router
