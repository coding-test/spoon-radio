const OAuth2Server = require('oauth2-server')
const { ErrorHandler } = require('helpers/error')

const { Request } = OAuth2Server
const { Response } = OAuth2Server

module.exports = async (req, res, next) => {

    const request = new Request(req);
    const response = new Response(res);

    try {
        const token = await req.app.oauth.token(request, response)
        return res.json(token)
    }catch(err) {
        throw new ErrorHandler(401, "CouldNotGenerateToken")
    }
}
