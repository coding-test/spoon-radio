const router = require('express').Router()
const controller = require('controllers/users')
const tokenAuth = require('middlewares/tokenAuth')

/* Authenticated routes */
router.use(
    [
        '/:target_user_id/block',
        '/:target_user_id/unblock',
    ],
    tokenAuth
)

router.post('/:target_user_id/block', controller.block)
router.post('/:target_user_id/unblock', controller.unblock)


/* Unauthenticated routes */

router.post('/login', controller.login)


module.exports = router
