exports.up = function(knex) {
    return knex.schema
        .createTable('blocking', function (table) {
            table.increments('id')
            table.integer('user_id').unsigned().notNull()
            table.integer('target_user_id').unsigned().notNull()

            table.foreign('user_id').references('id').inTable('users')
            table.foreign('target_user_id').references('id').inTable('users')

            table.unique(['user_id', 'target_user_id'])
        })
}

exports.down = function(knex) {
    return knex.schema
        .dropTable("blocking")
}
