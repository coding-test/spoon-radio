exports.up = function(knex) {
    return knex.schema
        .createTable('fan', function (table) {
            table.increments('id')
            table.integer('user_id').unsigned().notNull()
            table.integer('channel_id').unsigned().notNull()

            table.foreign('user_id').references('id').inTable('users')
            table.foreign('channel_id').references('id').inTable('channels')

            table.unique(['user_id', 'channel_id'])
        })
}

exports.down = function(knex) {
    return knex.schema
        .dropTable("fan")
}
