const clientTable = 'oauth_clients'
const tokenTable = 'oauth_tokens'

exports.up = async function(knex, Promise) {

    await knex.schema.createTable(clientTable, function(t) {
        t.increments('id').unsigned().primary()
        t.string('clientId').notNull()
        t.string('clientSecret').nullable()
        t.string('grants').nullable()
        t.string('redirectUris').nullable()
    })

    await knex.schema.createTable(tokenTable, function(t) {
        t.increments('id').unsigned().primary()
        t.string('accessToken').notNull()
        t.dateTime('accessTokenExpiresAt').nullable()
        t.string('refreshToken').nullable()
        t.dateTime('refreshTokenExpiresAt').nullable()
        t.integer('clientId').unsigned().notNull()
        t.foreign('clientId').references('id').inTable(clientTable)
        t.integer('userId').unsigned().nullable()
        t.foreign('userId').references('id').inTable('users')
    })

}

exports.down = async function(knex, Promise) {
    await knex.schema.dropTable(tokenTable)
    await knex.schema.dropTable(clientTable)
}

