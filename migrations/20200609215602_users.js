exports.up = function(knex) {
    return knex.schema
        .createTable('users', function (table) {
            table.increments('id')
            table.string('email').notNull()
            table.string('password').notNull()
            table.timestamps(true, true)
            table.timestamp('deleted_at').nullable()
        })
}

exports.down = function(knex) {
    return knex.schema
        .dropTable("users")
}
