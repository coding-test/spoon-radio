exports.up = function(knex) {
    return knex.schema
        .createTable('channels', function (table) {
            table.increments('id')
            table.integer('user_id').unsigned().notNullable()
            table.string('name').notNull()
            table.integer('fans').notNull().defaultTo(0)
            table.timestamp('started_at').nullable()
            table.timestamp('ended_at').nullable()
            table.timestamps(true, true)
            table.timestamp('deleted_at').nullable()

            table.foreign('user_id').references('id').inTable('users')

            table.unique(['user_id'])
        })
}

exports.down = function(knex) {
    return knex.schema
        .dropTable("channels")
}
